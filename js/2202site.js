$(document).ready(function(){
  var windowsize = $(window).width();
  var gal_h;

  //var countscroll for gallery phoneGallery
  var countScroll = 0;

  var bgcolor = $('body').css('color');
  var bmcolor = $('body').css('background');

  window.scrollTo(0, 0);

  ///////////////////
  ///draggable///////
  ///////////////////
  $(".container").sortable();
  $(".container").disableSelection();
  findSibling('.small_txt', 't');
  findSibling('img', 'i');
  findSibling('span', 's');
  findSibling('.work', 'w');
  $('.big_txt').each(function() {
    lineWrap(this);
  });
  placeObj('.small_txt');
  placeObj('img');
  placeObj('.work');
  placeObj('span');

  $(".elem").mousedown(function() {
    $(this).css("cursor", "grabbing");
    $(this).css("text-decoration", "underline");
  });
  $(".elem").mouseup(function() {
    $("div").css("cursor", "grab");
    $("div").css("text-decoration", "none");
  });

  $window = $(window);

  // animate content on scroll
  var lastScrollTop = 0;
  $window.scroll(function() {
    var scroll = $window.scrollTop();
    if (windowsize >= 800) {

      var height = 1;
      var percent = (scroll*100)/$('.grid').offset().top;
      var em = (height - (percent*height)/100);
      $(".big_txt").css('height',em+'em');

        if ( em < .1 ) {
            $("body").css('background', bgcolor);
            $(".grid").css('background', bgcolor);

            new SimpleBar($('.project')[0]);
            new SimpleBar($('.nav-container')[0]);
            $(".tags").show();
            $(".intro").hide();

            $('#verslesenfers').hide();
        }else{

            $("body").css('background', bmcolor);
            $(".grid").css('background', bmcolor);
            $(".tags").hide();
            $(".intro").show();
            $('#verslesenfers').show();

        }
    }else{
      if ( scroll > lastScrollTop ) {
        console.log("down");
        $(".phone-gallery").css("top",-scroll);

      }else{
        console.log("up");
        $(".phone-gallery").css("top",-scroll);
      }
      lastScrollTop = scroll;
    }
  });

  // menu filter
  var nav_array = [];
  $(".tags a").click(function(){
      var count = 0;
      $(this).toggleClass( "selected" );

      $("li").hide();
      $(".tags a").each(function(){
        if($(this).hasClass("selected")){
          var c = $(this).data("tag");
          $("li."+c).show();
          count ++;
        }
      });
      if(count == 0){
        $("li").show();
      }
  });

  //phone gallery animate
  if (windowsize <= 800) {
    $(".big_txt").each(function(){
      var index = Math.floor((Math.random() * 5) - 2);
      // console.log(index);
      $(this).css("z-index",index);
    })

    setTimeout ( function () {
       gal_h = $(".phone-gallery").outerHeight( true )+$(window).height();
       $(".phoneGalleryFooter").css('height', gal_h)
    }, 1500);
  }



  //retrieve content on click in the list
  $(".project-list").click(function(){
    var url = $(this).find(".project-url").data("url");

    var ttl=$(this).find('a');
    if (ttl.hasClass('opened')){
      ttl.removeClass('opened');
      $('#inside').html('');

    }else{
      $('.project-list a').removeClass('opened')
      ttl.addClass('opened');
      $.ajax({
        url: url,
        context: document.body,
        async:true,
        success: function(response){
          $('#inside').html(response);
          $('#inside img', this).load(function() {
            new SimpleBar($('#inside')[0]);
            new SimpleBar($('.nav-container')[0]);
          });


        }
      });
    }

    var ttl=$(this).find('h5');
    if (ttl.hasClass('opened')){
    ttl.removeClass('opened')
    }else{
      $('.project-list h5').removeClass('opened')
      ttl.addClass('opened')
    }


  });

var tryo=true;

$('#extra ul li a').on('mouseover', function(){
    var desc=$(this).attr('desc');
    $("#extra p").html(desc);
    tryo=false;
}).on('mouseout', function(){
    tryo=true;
    setTimeout(function(){
      if (tryo){
        $("#extra p").html("En dehors de Bonjours Monde et du Combo, nous menons aussi d'autres<br> aventures dont vous pouvez trouver ici une liste non exhaustive.<br>Survolez les liens pour une courte description.");
      }
    }, 300);
});

$('#extra-bout').on('click', function(){
    $("#extra").fadeToggle();
    $('#inside').html('');
    $('.pj-title, .project-url').removeClass('opened');
});

setTimeout(function(){
$('.ui-sortable-handle').each(function(){
  var childPos = $(this).offset();
  var parentPos = $('.container').offset();
  var childOffset = childPos.left - parentPos.left;
  var container = $(this).html();

  if (childOffset==0 && container=="&nbsp; "){
    $(this).remove();
  }
  });
}, 200);

//test api

var settings = {
  "async": true,
  "crossDomain": true,
  "url": "https://gitlab.com/api/v4/projects/13781978/repository/commits",
  "method": "GET",
  "headers": {
    "private-token": "Z2MVk9QdvQyBRS9VjhBH"
  }
}

$.ajax(settings).done(function (response) {
  console.log(response);
  console.log("d");
});


//slide genrator

$('.bonjourbox').on('click', function(){
    $(this).toggleClass("bonjourboxactivated");
});

//slide launcher
$('#boutslide').on('click', function(){
  slidzzzzzgenerator();
});

//codecache
var k = [83, 76, 73, 68, 69],
n = 0;
$(document).keydown(function (e) {
    if (e.keyCode === k[n++]) {
        if (n === k.length) {
          $('.nav').addClass('navslide');
          $('#popupslide, .bonjourbox').fadeIn();
          n = 0;
          slidesactivated=1;
          return false;
        }
    }
    else {
        n = 0;
    }
});
$(document).keydown(function (e) {
  if (e.keyCode =="27") {
    $('#ailegauche').html("");
    $('#popupslide, .bonjourbox, #slidecont').fadeOut();
    $('.bonjourbox').removeClass("bonjourboxactivated");
    $('.nav').removeClass('navslide');
    slidenb=0;
  }
});
slidzzzzz();
jumpscroll();

});

function lineWrap(obj) {
  var $cont = $(obj);
  var letter = $cont.html().split('');
  var cla = $cont.attr("class");
  for (i = 0; i < letter.length; i++) {
    if (letter[i] === " ") {
      letter[i] = '<div class="' + cla + '">' + "&nbsp;" + ' </div>';
    } else {
      letter[i] = '<div class="' + cla + '">' + letter[i] + ' </div>';
    }
  }
  //put the span into the container
  $cont.hide();
  $(".container").append(letter.join(''));
}

function findSibling(obj, letter) {
  var i = 0;
  $(obj).each(function() {
    i++;
    n = letter + i.toString();
    $(this).addClass(n);
    prev_elem = $(this)[0].previousElementSibling;
    $(prev_elem).addClass(n);
  });
}

function placeObj(obj) {
  $(obj).each(function() {
    var elems_b = [];
    var classes = $(this)[0].classList;
    var n = classes[classes.length - 1];
    $('.elem').each(function() {
      var elem = this;
      var classes_big = $(this)[0].classList;
      var n_b = classes_big[classes_big.length - 1];
      if (n_b === n) {
        elems_b.push(elem);
      }
    });
    $(this).insertAfter($(elems_b[elems_b.length - 1]))
  });
}


function goBack() {
    window.history.back();
}

function topFunction() {
  $('html,body').animate({ scrollTop: 0 }, 400);
  return false;
}


function phoneGallery(){
  var pic_src = [""];
  var pos_x, pos_y, pos_z;
  // change z_index of letters randomly
}
var slidenb=0;
var slidett=0;
var slidesactivated=0;

function slidzzzzz(){


    $("body").keydown(function(e) {
      if(slidesactivated){
      if(e.keyCode == 37) { // left
        slidenb-=1;
        if (slidenb<=0){slidenb=slidett}

        $('#ailedroite p').hide();
        $('#ailegauche img').hide();


        checkleg();
        checkchap();


        $('#ailedroite p:nth-child('+slidenb+')').fadeIn();
        $('#ailegauche img:nth-child('+slidenb+')').fadeIn();
      }
      else if(e.keyCode == 39) { // right
        slidenb+=1;
        if (slidenb>slidett){slidenb=1}

        $('#ailedroite p').hide();
        $('#ailegauche img').hide();

        checkleg();
        checkchap();

        $('#ailedroite p:nth-child('+slidenb+')').fadeIn();
        $('#ailegauche img:nth-child('+slidenb+')').fadeIn();
      }else if(e.keyCode == 38) { // up
        $('#infoslide').fadeOut();
      }
    }
    });

  function checkleg(){
      var checkproj=$("#chap").text();

      var titreproj=$('#ailegauche img:nth-child('+slidenb+')').attr('titreproj');
      var texteproj=$('#ailegauche img:nth-child('+slidenb+')').attr('texteproj');

      if (checkproj!=titreproj){
      $('#infoslide').fadeIn();
      $("#chap").html(titreproj);
      $("#texteproj").html(texteproj);
    }else{
      $('#infoslide').fadeOut();
    }
  }

  function checkchap(){
    var chap=$('#ailegauche img:nth-child('+slidenb+')').attr('titreproj');
    var achap=$("#chap").html(chap);
    if (chap && chap!=achap){
      $("#chap").html(chap);
    }
  }


    $('#ailedroite p').hide();
    $('#ailegauche img').hide();
    $('#chap span').hide();

    $('#ailedroite p:nth-child('+slidenb+')').fadeIn();
    $('#ailegauche img:nth-child('+slidenb+')').fadeIn();
    $('#chap span:nth-child('+slidenb+')').fadeIn();

}

function slidzzzzzgenerator(){

$('#slidecont').fadeIn();

$('.bonjourboxactivated').each(function(i) {
  var nextli=$(this).next();
  var url = nextli.find(".project-url").data("url");

  $.ajax({
    async: true,
    url: url,
    context: document.body,
    success: function(response){
      $('#ailegauchetemp').html(response);
      var titrethis=$("#ailegauchetemp > h1 > b").text();
      var textethis=$("#ailegauchetemp > p:nth-child(2)").text();

      console.log(titrethis);
      console.log(textethis);

      var imgs=$("#ailegauchetemp img");

      imgs.each(function(i){
        var tempurl=$(this).attr('src');
        if (tempurl.match(/\.(png|jpg|jpeg|gif)$/)){
        $('#ailegauche').append('<img src="'+tempurl+'" alt="" chap="'+i+'" titreproj="'+titrethis+'" texteproj="'+textethis+'">');
        }
      });

      $('#ailegauchetemp').html("");
    },
      error:function(err){
      alert(err.responseText)
    },
    complete:function(){
      slidett=$('#ailegauche>img').length;
      $('#ailegauche>img').hide();
    }
  });

});
}

var arewetop=true;

function jumpscroll(){
  var position = $(window).scrollTop();

  $(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if(scroll > position && arewetop) {
      if($(window).scrollTop() > 5){
            $("html, body").animate({ scrollTop: $(document).height() }, 1500);
            $("#extra").fadeOut();
            arewetop=false;
        }
    } else if (scroll<5){
      arewetop=true;
    }
    position = scroll;
});
}
